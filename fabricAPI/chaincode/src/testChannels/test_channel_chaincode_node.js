/*
# Copyright IBM Corp. All Rights Reserved.
#
# SPDX-License-Identifier: Apache-2.0
*/

const shim = require('fabric-shim');
const util = require('util');

var Chaincode = class {

  // Initialize the chaincode
  async Init(stub) {
    console.info('========= caseList Init =========');
    let caseList = [];
    try {
      await stub.putState('caseList', Buffer.from(JSON.stringify(caseList)));
      return shim.success();
    } catch (err) {
      return shim.error(err);
    }
  }

  async Invoke(stub) {
    let ret = stub.getFunctionAndParameters();
    console.info(ret);
    let method = this[ret.fcn];
    if (!method) {
      console.log('no method of name:' + ret.fcn + ' found');
      return shim.success();
    }
    try {
      let payload = await method(stub, ret.params);
      return shim.success(payload);
    } catch (err) {
      console.log(err);
      return shim.error(err);
    }
  }

  async createCase(stub, args){
    let caseObj = JSON.parse(args[0])
    await stub.putState(caseObj.caseId, Buffer.from(JSON.stringify(caseObj)));
    let caseList = await stub.getState('caseList');
    caseList = JSON.parse(caseList);
    caseList.push(caseObj.caseId)
    await stub.putState('caseList', Buffer.from(JSON.stringify(caseList)));
    stub.setEvent('esputo');
    return caseObj.caseId
  }

  async getCases(stub, args){
    let caseList = await stub.getState("caseList");
    caseList = JSON.parse(caseList);
    let allCases = []
    console.log('[...getCases...]', caseList.join(', '))
    for(var i=0; i<caseList.length; i++){
      let caseObj = await stub.getState(caseList[i]);
      caseObj = JSON.parse(caseObj);
      allCases.push(caseObj)
    }
    return Buffer.from(JSON.stringify(allCases))
  }

  async getCase(stub, args) {
    let caseId = args[0]
    let caseObj = await stub.getState(caseId);
    return Buffer.from(caseObj);
  }

  async acceptCase(stub, args){
    let caseId = args[0]
    let status = args[1]
    let propagate = args[2]
    let caseObj = await stub.getState(caseId);
    caseObj = JSON.parse(caseObj);
    caseObj.status = JSON.parse(status);
    await stub.putState(caseObj.caseId, Buffer.from(JSON.stringify(caseObj)));
    let eventName = "caseAccepted";
    console.info('....setEvent', eventName)
    let payload = Buffer.from(JSON.stringify(caseObj))
    if (propagate == 'true'){
      eventName = "caseAcceptedToPropagate"
    }
    stub.setEvent(eventName, payload)
    console.info('[acceptCase] OK',)
    return Buffer.from(caseObj.caseId);
  }


  async declineCase(stub, args){
    let caseId = args[0]
    let status = args[1]
    let propagate = args[2]
    let caseObj = await stub.getState(caseId);
    caseObj = JSON.parse(caseObj);
    caseObj.status = JSON.parse(status);
    await stub.putState(caseObj.caseId, Buffer.from(JSON.stringify(caseObj)));
    let eventName = "caseDeclined";
    console.info('....setEvent', eventName)
    let payload = Buffer.from(JSON.stringify(caseObj))
    if (propagate == 'true'){
      eventName = "caseDeclinedToPropagate"
    }
    stub.setEvent(eventName, payload)
    console.info('[declineCase] OK',)
    return Buffer.from(caseObj.caseId);
  }


  async rejectCase(stub, args){
    let caseId = args[0]
    let status = args[1]
    let propagate = args[2]
    let caseObj = await stub.getState(caseId);
    caseObj = JSON.parse(caseObj);
    caseObj.status = JSON.parse(status);
    await stub.putState(caseObj.caseId, Buffer.from(JSON.stringify(caseObj)));
    let eventName = "caseRejected";
    console.info('....setEvent', eventName)
    let payload = Buffer.from(JSON.stringify(caseObj))
    if (propagate == 'true'){
      eventName = "caseRejectedToPropagate"
    }
    stub.setEvent(eventName, payload)
    console.info('[rejectCase] OK',)
    return Buffer.from(caseObj.caseId);
  }

  async updateCase(stub, args) {
    let caseId = args[0]
    let caseObj = JSON.parse(args[1]) || {}
    let propagate = args[2]
    //let eventName = args[2]
    let caseObjOrigin = await stub.getState(caseId);
    /////////////////////////
    // Create if not existed
    /////////////////////////
    if (caseObjOrigin.toString().length === 0) {
      console.info('....Create if not existed')
      let caseList = await stub.getState('caseList');
      caseList = JSON.parse(caseList);
      caseList.push(caseId)
      await stub.putState('caseList', Buffer.from(JSON.stringify(caseList)));
    }

    ///////////////////
    // Updating caseId
    ///////////////////
    if (caseObj.caseId && caseId !== caseObj.caseId) {
      console.info('....Updating caseId')
      await stub.putState(caseId, '');
      let caseList = await stub.getState('caseList');
      caseList = JSON.parse(caseList);
      for (var i=0; i<caseList.length; i++){
        if (caseList[i] == caseId){
          caseList[i] = caseObj.caseId;
          break;
        }
      }
      await stub.putState('caseList', Buffer.from(JSON.stringify(caseList)));
      caseId = caseObj.caseId;
    }
    //////////////////////////////////////////
    // Update or create
    //////////////////////////////////////////
    caseId = args[0]
    let payload
    let ncase
    let ncaseBuffer = await stub.getState(caseId)
    if (ncaseBuffer.toString().length !== 0) {
      ////////////////////////////////
      // Update only new keys (PATCH)
      ////////////////////////////////
      ncase = JSON.parse(ncaseBuffer)
      Object.keys(caseObj).map(key => {
        console.log('...key:', key)
        if (!caseObj.caseId && key === 'status') {
          Object.keys(caseObj[key]).map(skey => {
            // Not update caseOwner and corporate unit because this transaction
            // is propagating from local units not from corporate unit or caseOwner
            // if (skey !== ncase.caseOwner && skey !== 'corporation') {
              ncase[key][skey] = caseObj[key][skey]
            // }
          })
        } else {
          ncase[key] = caseObj[key] // update all attachments and stuff!
        }
      })
      payload = Buffer.from(JSON.stringify(ncase))
    } else if (caseObj.caseId) {
      //////////
      // CREATE
      //////////
      console.log('....Create case', caseObj.caseId)
      payload = Buffer.from(JSON.stringify(caseObj))
    }
    //////////
    // UPDATE
    //////////
    let eventName = "caseUpdated"
    await stub.putState(caseId, payload);
    console.info('....setEvent', eventName)
    if (propagate == 'true'){
      eventName = "caseUpdatedToPropagate"
      stub.setEvent(eventName, payload)
    }else{
      stub.setEvent(eventName, payload)
    }
    console.info('[updateCase] OK',)
    return Buffer.from(caseId)
  }
};

shim.start(new Chaincode());
